#!/usr/bin/python3

import os
import sys
import shutil
import argparse

def parseConfigFile(configFile):

    configLines = open(configFile).readlines()
    histConfigs = {}
    for configLine in configLines:
        configLine = configLine.strip().replace(" ", "").replace("\t", "")
        histLegend = configLine.split(",")[0]
        histDirectories = configLine.split(",")[1].split("+")
        histConfigs[histLegend] = histDirectories

    return histConfigs

def makeRivetCommands(histConfigs, genvalId):

    rivetLines = ""
    for histLegend in list(histConfigs.keys()):
        histStack = " "
        for histDirectory in histConfigs[histLegend]:
            hist = os.path.join(histDirectory, "merged.yoda.gz")
            if not os.path.exists(histDirectory):
                sys.exit("[error] {0} does not exist, check the config file".format(histDirectory))
            if not os.path.exists(hist):
                sys.exit("[error] {0} does not exist, check the directory".format(os.path.join(histDirectory, "merged.yoda.gz")))
            histStack += "{0} ".format(hist)
        rivetLines += "yodastack -M ratio -o tmp_{0} {1}\n".format(os.path.join(genvalId, histLegend + ".yoda.gz"), histStack)

    return rivetLines

def main():

    parser = argparse.ArgumentParser("Generator validation scripts")

    parser.add_argument("-c", "--config",\
                        action="store", dest="config", default=None,\
                        help = "missing histogram configuration file")
    parser.add_argument("--stack",\
                        action="store_true", dest="stack", default=False,\
                        help = "draw stacked histograms (without mc errors), useful for incl vs excl")
    parser.add_argument("--merge",\
                        action="store_true", dest="merge", default=False,\
                        help = "draw merged histograms (with mc errors), useful for 1 vs 1")

    args = parser.parse_args()

    configFile = args.config
    if not (args.stack or args.merge):
        sys.exit("[error] --stack or --merge should be used")

    genvalId = hex(id(configFile))
    os.system("mkdir tmp_{0}".format(genvalId))

    histConfigs = parseConfigFile(configFile)
    rivetLines = makeRivetCommands(histConfigs, genvalId)
    os.system(rivetLines)

    rivetCommand = ""
    rivetCommand += " rivet-mkhtml tmp_{0}/*.yoda.gz".format(genvalId)
    
    if not shutil.which("latex"):
        rivetCommand = "singularity exec -B $PWD:$PWD docker://hepstore/rivet {0}".format(rivetCommand)

    if args.stack:
        os.system("{0} --no-ratio --no-weights -o {1}_stacked".format(rivetCommand, configFile.replace(".", "_")))
        os.system("tar -czf {0}_stacked.tar.gz --remove-files {1}_stacked".format(configFile.replace(".", "_"), configFile.replace(".", "_")))
    elif args.merge:
        os.system("{0} -o {1}_merged".format(rivetCommand, configFile.replace(".", "_")))
        os.system("tar -czf {0}_merged.tar.gz --remove-files {1}_merged".format(configFile.replace(".", "_"), configFile.replace(".", "_")))

    os.system("rm -rf tmp_{0}".format(genvalId))

if __name__ == '__main__':

    main()

