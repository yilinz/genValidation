import os
import sys

from .steerGen import steer as steerGen

def addCmsBuild(args, runLines):

    ll = open("{0}".format(os.path.join("config", "run", "cmsenv.dat"))).readlines()

    configs = {}
    for l in ll:
        exec(l, globals())

    runLines += "source /cvmfs/cms.cern.ch/cmsset_default.sh\n"
    runLines += "export SCRAM_ARCH={0}\n".format(SCRAM_ARCH)
    runLines += "scramv1 project CMSSW {0}\n".format(CMSSW)
    runLines += "cd {0}/src/\n".format(CMSSW)
    runLines += "eval `scramv1 runtime -sh`\n"

    runLines += "mkdir -p Configuration/GenProduction/python/\n"
    runLines += "mv ../../fragment.py Configuration/GenProduction/python/\n"
    runLines += "mv ../../rivet.py Configuration/GenProduction/python/\n"
    runLines += "mv ../../random.py Configuration/GenProduction/python/\n"

    # move the uploaded gridpack in the running directory if the job queue is cmsconnet
    if args.queue == "cmsconnect":
        runLines += "mv ../../{0} ./\n".format(args.gridpack.split("/")[-1])
    runLines += "scram b\n"

    return runLines

def addKeepOutput(runLines, keeppath):

    if keeppath:
        runLines += "mkdir -p {0}".format(os.path.join(keeppath, runpointDir))
        runLines += "mv genVal.root {0}/".format(os.path.join(keeppath, runpointDir))
    else:
        runLines += "mv genVal.root ../../"

    return runLines

def getRun(args, useLHE, runpointDir):

    runLines = "#!/bin/bash\n"

    runLines = addCmsBuild(args, runLines)
    if not args.keep:
        runLines = steerGen(runLines, args.nevents, useLHE)
    else:
        if args.keep == "GEN":
            runLines = steerGen(runLines, args.nevents, useLHE)
        else: 
            sys.exit("[error] getRun unknown")
#        elif args.keep == "NanoGEN": #TODO
#            pass
#        elif args.keep == "DQM": #TODO
#            pass

        runLines = addKeepOutput(runLines, args.keeppath, runpointDir)

    runLines += "cmsRun run.py 2> result.log\n"
    runLines += "mv result.log ../../\n"
    # remove ratio hists from yoda, otherwise issues with yodastack
    runLines += "yoda2yoda -M ratio rivet_result.yoda\n"
    runLines += "mv rivet_result.yoda ../../\n"

    return runLines


def checkExternalLHEProducer(runpointDir):

    fragmentLines = open(os.path.join(runpointDir, "fragment.py")).readlines()

    for fragmentLine in fragmentLines:
        if "externalLHEProducer" in fragmentLines:
            return False

    return True


def main(args, runpointDir):

    useLHE = checkExternalLHEProducer(runpointDir)

    runLines = getRun(args, useLHE, runpointDir)
    runFile = open("{0}".format(os.path.join(runpointDir, "run.sh")), "w")
    runFile.write(runLines)
    runFile.close()

    os.chmod(os.path.join(runpointDir, "run.sh"), 0o755)

