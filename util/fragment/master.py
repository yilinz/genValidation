import os
import sys
import random

from .steerMadgraph import steer as steerMadgraph

def addExtLHEProducer(fragmentLines):

    fragmentLines +="""
import FWCore.ParameterSet.Config as cms

externalLHEProducer = cms.EDProducer(\"ExternalLHEProducer\",
\targs = cms.vstring(\"[gridpack]\"),
\tnEvents = cms.untracked.uint32([nevents]),
\tnumberOfParameters = cms.uint32(1),
\toutputFile = cms.string('cmsgrid_final.lhe'),
\tscriptName = cms.FileInPath('GeneratorInterface/LHEInterface/data/run_generic_tarball_cvmfs.sh')
)

"""
    return fragmentLines


def addFragment(fragmentLines, fragment):

    fragmentLines += open(fragment).read()

    return fragmentLines


def getFragment(args):

    fragmentLines = ""

    # fetch extLHEProducer fragment if needed
    if args.mceg in ["madgraph", "powheg"]:
        fragmentLines = addExtLHEProducer(fragmentLines)
    else:
        pass

    fragmentLines = addFragment(fragmentLines, args.fragment)

    # specicial treatment on cmsconnect gridpack
    if args.queue == "cmsconnect":
        gridpack = "../{0}".format(args.gridpack.split("/")[-1])
    else:
        gridpack = args.gridpack

    # generator dependent steering scripts
    if args.mceg == "madgraph":
        fragmentLines = steerMadgraph(fragmentLines, gridpack, args.nevents)
    else: #TODO work on different generators
        sys.exit("[error] getFragment unknown")

    return fragmentLines


def main(args, runpointDir):

    fragmentFile = open(os.path.join(runpointDir, "fragment.py"), "w")
    fragmentLines = getFragment(args)
    fragmentFile.write(fragmentLines)
    fragmentFile.close()

    os.system("cp {0} {1}".format(os.path.join("config", "fragment", "random.py"), os.path.join(runpointDir, "random.py")))


